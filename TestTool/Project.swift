//
//  Project.swift
//  TestTool
//
//  Created by Nicholas Galasso on 7/5/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation

struct Project {
    
    var title:String
    var selfURL:String
    
    static func projectFromDictionary(dict:Dictionary<String,AnyObject>) -> Project? {
        
        let links = dict["links"] as! Dictionary<String,AnyObject>
        let meta = dict["metadata"] as! Dictionary<String,AnyObject>
        
        guard let selfURL = links["self"] as? NSString,
            let type = meta["project_type"] as? NSString,
            let title = meta["title"] as? NSString
            else {
                assert(false)
                return nil
        }
        
        if type.isEqualToString("video_slideshow") {
            
            let newProj = Project(title: title as String, selfURL: selfURL as String)
            return newProj
            
        } else {
            return nil
        }
    }
    
}