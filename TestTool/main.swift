//
//  main.swift
//  TestTool
//
//  Created by Nicholas Galasso on 6/30/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation

print("Hello, World!")

var queue = NSOperationQueue()

var shouldRun = true

while shouldRun {
    
    let possibleResponse = readLine(stripNewline: true)
    
    if let response = possibleResponse {
        
        switch response {
        
        case "projects":
            
            let op = GetProjectListOperation(handler: { (success, list) -> Void in
                if (success) {
                    
                    print("got projects:")
                    for proj in list {
                        print("\(proj.title) \(proj.selfURL)")
                    }
                    
                } else {
                    print("could not get project list")
                }
            })
            
            queue.addOperation(op)
            
        case let r where r.containsString("http"):
            
            let op = GetProjectOperation(url: r, handler: { (success, project) in
                if (success){
                    print(project!)
                } else {
                    print("could not get project at: \(r)")
                }
            })
            
            queue.addOperation(op)
            
        case "exit":
            shouldRun = false
            
        default:
            print("unrecognized: \(response)")
        }
    }
}

print("OK bye");
