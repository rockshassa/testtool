//
//  GetProjectsOperation.swift
//  TestTool
//
//  Created by Nicholas Galasso on 7/5/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation

typealias GetProjectListHandler = (success:Bool, list:[Project]) -> Void

class GetProjectListOperation: NSOperation, BuilderOperation {
    
    let completionHandler:GetProjectListHandler
    
    init(handler:GetProjectListHandler){
        completionHandler = handler
        super.init()
    }
    
    override func main() {
        
        let req = self.dataTaskForURL("https://app-service.animoto.com/projects?page_number=1&page_size=50", httpMethod: "GET" , body: nil) { (data, resp, err) -> Void in
            
            if let r = resp as? NSHTTPURLResponse {
                
                var success = false
                
                switch (r.statusCode){
                    case 200, 201:
                        success = true
                    default:
                        print("request failed")
                }
    
                guard let projects = self.parseData(data)?["response"]?["payload"]?!["projects"] as? [NSDictionary] else {
                    print("get project list failed")
                    return
                }
                
                var list:[Project] = []
                
                for dict in projects {
                    if let p = Project.projectFromDictionary(dict as! Dictionary<String, AnyObject>) {
                        list.append(p)
                    }
                }
                
                self.completionHandler(success: success, list:list)
            }
        }
        
        req.resume()
    }
}