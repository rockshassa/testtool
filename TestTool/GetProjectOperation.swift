//
//  GetProjectOperation.swift
//  Builder
//
//  Created by Nicholas Galasso on 12/7/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

import Foundation

typealias GetProjectHandler = (success:Bool, project:NSDictionary?) -> Void

class GetProjectOperation : NSOperation, BuilderOperation {
    
    var projURL:String
    var completionHandler:GetProjectHandler
    
    init(url:String, handler:GetProjectHandler){
        self.projURL = url
        self.completionHandler = handler
    }
    
    override func main(){
        let req = self.dataTaskForURL(self.projURL as String!, httpMethod: "GET", body: nil) {
            
            (data, resp, err) -> Void in
            
            if let r = resp as? NSHTTPURLResponse {
                
                var success = false;
                
                switch (r.statusCode){
                    case 200, 201:
                        success = true
                    default:
                        print(r)
                }
                
                guard let payload = self.parseData(data)?["response"]?["payload"] as? NSDictionary where success else {
                    print("get project failed")
                    return
                }
                
                self.completionHandler(success: success, project: payload)
            }
        }
        
        req.resume()
    }
    
}