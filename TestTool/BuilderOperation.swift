//
//  BuilderOperation.swift
//  TestTool
//
//  Created by Nicholas Galasso on 7/5/16.
//  Copyright © 2016 Rockshassa. All rights reserved.
//

import Foundation

typealias RequestCompletionHandler = (data:NSData?, resp:NSURLResponse?, err:NSError?) -> Void

protocol BuilderOperation {
    func dataTaskForURL(urlString:String, httpMethod:String, body:AnyObject?, completion:RequestCompletionHandler) -> NSURLSessionDataTask
    func parseData(data:NSData?) -> NSDictionary?
}

extension BuilderOperation where Self:NSOperation {
    
    func dataTaskForURL(urlString:String, httpMethod:String, body:AnyObject?, completion:RequestCompletionHandler) -> NSURLSessionDataTask {
        
        guard let url = NSURL(string: urlString) else {
            print("couldnt form URL from \(urlString)")
            assert(false)
            return NSURLSessionDataTask()
        }
        
        let req = NSMutableURLRequest(URL:url, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: 10)
        
        let headers = [
            "Content-type":"application/vnd.animoto-v4+json",
            "Accept":"application/vnd.animoto-v4+json",
            "Authorization":"Bearer 30f52cbf0fcb1d160bd1b0140b3e1f0c49b9e4a3efb6adda1093c5d32d0490d2",
            "User-Agent":"Simulator (OS 9.3) (7EMYPGVGGG.com.animoto.slideshow 7.8.0_go_live) (app_service 2.0)"
        ]
        
        req.allHTTPHeaderFields = headers;
        req.HTTPMethod = httpMethod
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(req, completionHandler: completion)
        
        return task
    }
    
    func parseData(data:NSData?) -> NSDictionary? {
        
        guard (data != nil) else {
            return nil
        }
        
        do {
            
            let obj = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers)
            return obj as? [String : AnyObject]
            
        } catch {
            print("json error: \(error)")
            assert(false)
            return nil
        }
    }
}